# Comparative analysis of speech to text systems for Ukrainian dialects



## Project description

Automated Speech Recognition (ASR) or Speech to Text (STT) task still takes a leading position in discussions about tools for investigation of Ukrainian language and its practical usage in various tech products. 
In this research we will try to find out which Ukrainian dialect shows the best results in recognition by different Ukrainian ASR models.
For the testing we took audios from 16 Ukrainian regions: Chernihiv, Ternopil, Poltava, Luhansk, Zakarpattya, Vinnytsia, Cherkasy, Sumy, Mykolayiv, Kyiv, Zhytomyr, Khmelnytskyi, Rivno, Lviv, Ivano-frankivsk and Volyn.
We conducted the tests on 10 Ukrainian STT models: DeepSpeech, Wav2Vec and Nemo

## Project files
This repository contains all data and scripts that were used in experiments
Also here you can find all resulting files

## Manualy created transcript 
transcriptions.xlsx file contain all testing data info with audio links, length and transcriptions

## Testing scripts 
Testing scripts for DeepSpeech, Wav2Vec and Nemo models can be found in python notebooks

## Models result transcriptions 
result_transcriptions folder contains excel files with models' results and WER, CER metrics for each of 16 dialects

## Testing audio
Audio_dialects folder contains folders with audio files for each of 16 dialects

